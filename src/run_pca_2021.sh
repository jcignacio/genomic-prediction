#!/bin/bash

# Generate taxa list to include for TASSEL PCA
awk -F '\t' '!seen[$3]++' data/2021_NorGrains_full_key_ILINKYOH_PCA_LINES.txt > tmp/2021_NorGrains_full_key_ILINKYOH_PCA_LINES_deduped.txt
awk -F '\t' '{if (NR>1) printf("%s ",$3)}' tmp/2021_NorGrains_full_key_ILINKYOH_PCA_LINES_deduped.txt > tmp/pca_include_lines.txt

# Identify markers in high LD
plink --double-id --allow-extra-chr --vcf data/2021_NORG_KY_IN_OH_postimp_filt.vcf.gz --make-bed --out tmp/2021_NORG_KY_IN_OH_postimp_filt
plink --bfile tmp/2021_NORG_KY_IN_OH_postimp_filt --indep-pairwise 100 10 0.90 --allow-extra-chr --make-founders --out tmp/2021_NORG_KY_IN_OH_postimp_filt

# Convert list of markers to include to TASSEL format
tr '\n' ' ' < tmp/2021_NORG_KY_IN_OH_postimp_filt.prune.in > tmp/pca_include_markers.txt

# TASSEL location
tassel=~/softwares/tassel-5-standalone/run_pipeline.pl 

# Run PCA in TASSEL using OSC
srun -c4 -A PAS1286 $tassel -Xmx24g -vcf data/2021_NORG_KY_IN_OH_postimp_filt.vcf.gz \
-includeSiteNamesInFile tmp/pca_include_markers.txt \
-includeTaxaInFile tmp/pca_include_lines.txt \
-export tmp/2021_NORG_KY_IN_OH_postimp_filt_pcalines_pruned.hmp.txt \
-exportType HapmapDiploid

srun -c24 -A PAS1286 $tassel -Xmx24g -h tmp/2021_NORG_KY_IN_OH_postimp_filt_pcalines_pruned.hmp.txt \
-PrincipalComponentsPlugin -covariance true -endPlugin -export output/2021_pca_output &

srun -c24 -A PAS1286 $tassel -Xmx24g -vcf data/2021_NORG_KY_IN_OH_postimp_filt.vcf.gz \
-includeSiteNamesInFile tmp/pca_include_markers.txt \
-includeTaxaInFile tmp/pca_include_lines.txt \
-PrincipalComponentsPlugin -covariance true -endPlugin -export output/2021_pca_output &

# Run PCA in TASSEL using local machine

$tassel -Xmx4g -vcf data/2021_NORG_KY_IN_OH_postimp_filt.vcf.gz \
-includeSiteNamesInFile tmp/pca_include_markers.txt \
-export tmp/2021_NORG_KY_IN_OH_postimp_filt_pruned.hmp.txt \
-exportType HapmapDiploid

$tassel -Xmx4g -h tmp/2021_NORG_KY_IN_OH_postimp_filt_pruned.hmp.txt \
-includeTaxaInFile tmp/pca_include_lines.txt \
-export tmp/2021_NORG_KY_IN_OH_postimp_filt_pcalines_pruned.hmp.txt \
-exportType HapmapDiploid

$tassel -Xmx3g -vcf data/2021_NORG_KY_IN_OH_postimp_filt.vcf.gz \
-PrincipalComponentsPlugin -covariance true -endPlugin -export output/2021_pca_output &

$tassel -Xmx4g -vcf data/2021_NORG_KY_IN_OH_postimp_filt.vcf.gz \
-includeSiteNamesInFile tmp/pca_include_markers.txt \
-includeTaxaInFile tmp/pca_include_lines.txt \
-PrincipalComponentsPlugin -covariance true -endPlugin -export output/2021_pca_output &