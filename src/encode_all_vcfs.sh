#!/bin/bash

tassel=~/workspace/tassel-5-standalone/run_pipeline.pl # Path to TASSEL5
data_path=data # Path were gzipped VCF files are located
encoder=src/bglr_encode_v3.pl # Script for encoding, bglr_encode_v3.pl (Juanillas and Ulat, 2017)

mkdir -p tmp

# Convert VCF to Hapmap then encode
for vcf_file in ${data_path}/*.vcf.gz
	do
		bn=$(basename $vcf_file)
		n=${bn%.vcf.gz}
		$tassel -Xmx2g -vcf $vcf_file -export tmp/${n}.hmp.txt -exportType HapmapDiploid
		perl $encoder '2letter' tmp/${n}.hmp.txt '0,1,2,NA' tmp/${n}.gt.enc.txt &
	done

# Clean up
for vcf_file in ${data_path}/*.vcf.gz
	do
		bn=$(basename $vcf_file)
		n=${bn%.vcf.gz}
		rm tmp/${n}.hmp.txt
	done

# Correct sample names in genotype file
# for gt_file in tmp/*.gt.enc.txt
#	do
#		bn=$(basename $gt_file)
#		n=${bn%.gt.enc.txt}
#		Rscript src/correct_sample_names.R $n
#	done
