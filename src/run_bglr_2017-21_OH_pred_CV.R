#!/usr/bin/env Rscript
#install.packages("BGLR")
#install.packages("plyr")
library(BGLR)
library(tidyverse)
library(plyr)
library(readxl)

wd <- "/home/carli/workspace/genomic-prediction/"
# locs <- c("OH","KY","IN")

args = commandArgs(trailingOnly=TRUE)
prefix <- ifelse(length(args) > 0, args[1], "2021_all_NorGrains+historical_NIFA_imp")
if(length(args) == 0){
  setwd(wd)
}

# oh_excel_file <- "data/2021 BLUES IL-IN-KY-OH Lines.xlsx"
oh_phenotype_file <- "data/2017-2021 OH BLUPS.csv"
# in_phenotype_file <- "data/in_blues_2020.csv"
# ky_phenotype_file <- "data/ky_blues_2020.csv"
key_file <- "data/2021_NIFA_states_merged_filt_imputed_keyfile.csv"

encoded_gt_zip <- paste0("tmp/",prefix,".gt.enc.zip")
encoded_gt <- paste0("tmp/",prefix,".gt.enc.RENAMED.txt")

cat("Loading phenotypes... ")
# oh_phenos <- lapply(2:5,FUN = function(x) read_xlsx(oh_excel_file,sheet=x))
# oh_df <- lapply(oh_phenos, data.frame)
# oh_df <- lapply(oh_df, function(x) {colnames(x)[1] <- "NAME"; return(x)})
# 
# oh_pheno <- oh_df %>% reduce(full_join, by="NAME")
# #colnames(oh_pheno) <- c("NAME","YIELD","TW","FHB.IND")

oh_pheno <- read.csv(oh_phenotype_file,sep=",",stringsAsFactors = F, header = T)
# in_pheno <- read.csv(in_phenotype_file,sep="\t",stringsAsFactors = F, header = T)
# ky_pheno <- read.csv(ky_phenotype_file,sep=",",stringsAsFactors = F, header = T, row.names = 1)
merged <- oh_pheno

## Resolve conflicting headers, if any
# add prefix in header names
# add_prefix <- function(name, prefix, delimiter="."){
#   paste0(c(prefix,name),collapse = ".")
# }
# 
# colnames(oh_pheno) <- sapply(colnames(oh_pheno),add_prefix,"OH")
# colnames(in_pheno) <- sapply(colnames(in_pheno),add_prefix,"IN")
# colnames(ky_pheno) <- sapply(colnames(ky_pheno),add_prefix,"KY")
# 
# # add prefix to accession names
# oh_pheno[,"UNIQUE.NAME"] <- sapply(oh_pheno[,"OH.X"],add_prefix,"OH")
# in_pheno[,"UNIQUE.NAME"] <- sapply(in_pheno[,"IN.Lines"],add_prefix,"IN")
# ky_pheno[,"UNIQUE.NAME"] <- sapply(ky_pheno[,"KY.name"],add_prefix,"KY")
# 
# # make column to preserve name of all locations
# oh_pheno[,"NAME"] <- oh_pheno[,"OH.X"]
# in_pheno[,"NAME"] <- in_pheno[,"IN.Lines"]
# ky_pheno[,"NAME"] <- ky_pheno[,"KY.name"]
# 
# # Merge all data
# merged <- rbind.fill(rbind.fill(oh_pheno,in_pheno),ky_pheno)
# cat(dim(merged)[1],"entries loaded!\n")
# # colnames(merged)

# Make output names for file
kf <- read.csv(key_file,sep=",",stringsAsFactors = F,header=T)
kf$names_out <- kf$FullSampleName

# Encode genotype file
# run 'data/encode_vcf.sh'

# Read and check encoded genotype file
source("src/gtutils.R")

if(!file.exists(encoded_gt)){
  unzip(zipfile=encoded_gt_zip, exdir="./data/")
}
cat("Loading genotype file... ")
enc <- load_gt(encoded_gt)
cat(dim(enc)[2]-1, "samples and", dim(enc)[1]-12, "markers loaded!\n")

### Prepare GS dataset
cat("Preparing GS dataset...\n")

# Get sample names
sample_names <- get.sample.names(enc)

matching_sample_names <- match.acc.names(merged$"NAME",sample_names)

# Match phenotypes with genotypes
pheno_with_gt <- merged[!is.na(matching_sample_names),]
colSums(!is.na(pheno_with_gt))
table(substr(pheno_with_gt$NAME,1,2))

# Set list of traits to analyze
names(pheno_with_gt)
(traits <- names(pheno_with_gt)[c(2:7)])

# Assign genotype matrix
X <- prepare_input_gt(enc)

# Trim pheno and geno data
# trimmed.y <- pheno_with_gt[rowSums(is.na(pheno_with_gt[traits])) != length(traits),]
X <- X[match.acc.names(pheno_with_gt$"NAME",sample_names),]



# 
# # # Specify trait
# # ky.fhb.y <- pheno_with_gt[!is.na(pheno_with_gt[,"KY.fhb"]),"KY.fhb"]
# # ky.fhb.x <- X[!is.na(pheno_with_gt[,"KY.fhb"]),]
# # 
# # # Run CV
# # yhat <- run_cv(ky.fhb.y,ky.fhb.x,10)
# # 
# # # Get accuracy
# # cor(ky.fhb.y,yhat)
# 
# yhat.trt1 <- run_cv(y = trimmed.y[,traits[1]], X = trimmed.x, nfold = 10)
# cor(trimmed.y[,traits[1]],yhat.trt1)
# yhat.trt2 <- run_cv(y = trimmed.y[,traits[2]], X = trimmed.x, nfold = 10)
# cor(trimmed.y[,traits[2]],yhat.trt2)

library(parallel)

fitFold=function(y,W,folds,fold){
  tst=which(folds==fold)
  yNA=y
  yNA[tst]=NA
  fm=BGLR(y=yNA,ETA=list(list(X=W,model='BRR')),nIter=1500,burnIn=500,verbose=F)
  tmp=fm$yHat[tst]
  names(tmp)=tst
  return(tmp)
}

run_mc_cv <- function(trait,pheno_with_gt){
  trait.y <- pheno_with_gt[!is.na(pheno_with_gt[,trait]),trait]
  trait.x <- X[!is.na(pheno_with_gt[,trait]),]
  folds=sample(1:10,size=length(trait.y),replace=T)
  
  cat("Running", trait, "...\n")
  print(system.time( tmp<-mclapply(FUN=fitFold,y=trait.y,W=trait.x,folds=folds,X=1:10,mc.cores=10)))
  yHatCV2=unlist(tmp)
  yHatCV2=yHatCV2[order(as.integer(names(yHatCV2)))] # need to order the vector
  acc <- cor(trait.y,yHatCV2)
  cat("CV accuracy for", trait,"is", acc, ".\n")
  return(acc)
}

# pred_out <- lapply(pheno_with_gt[traits],run_bglr,X,TRUE)
# df_pred <- do.call(cbind,pred_out)
# colnames(df_pred) <- paste("pred",colnames(df_pred),sep=".")
# write.csv(cbind(merged$NAME,df_pred[match(merged$NAME,pheno_with_gt$NAME),]),"output/2021-IL-IN-KY-OH-Lines-predictions-mergedGT.csv")

cv_out <- lapply(traits,run_mc_cv,pheno_with_gt)
df_cv <- data.frame(trt = traits, cv.10.fold.acc = do.call(rbind,cv_out))
write.csv(df_cv,"output/2017-21-OH-pred-CV.csv")
