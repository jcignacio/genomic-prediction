setwd("~/workspace/genomic-prediction")

source("src/gtutils.R")

# Load data
gt <- read.table("tmp/genfreq.gt.enc.txt",header=F,comment.char = "%")
sm <- read.csv("data/sample_keys.csv")
tw <- read.csv("data/5-state-twt.csv", na.strings=c("","NA","."))
yl <- read.csv("data/5-state-yld.csv", na.strings=c("","NA","."))
oh.blues <- read.csv("data/oh_blups.csv",sep=",",stringsAsFactors = F, header = T)
in.blues <- read.csv("data/in_blups.csv",sep="\t",stringsAsFactors = F, header = T)
ky.blues <- read.csv("data/ky_blues_2020.csv",sep=",",stringsAsFactors = F, header=T)
il.blues <- read.csv("data/il_blups.csv",sep=",",stringsAsFactors = F, header=T)

# Rename columns and merge YLD and TW data
names(tw)[-1] <- paste("tw.",names(tw), sep="")[-1]
names(yl)[-1] <- paste("yl.",names(yl), sep="")[-1]
pheno <- merge(tw,yl,by="X")

# Exclude unwanted samples
samples.to.exclude <- c("0762A12-8")
#samples.to.exclude <- c(samples.to.exclude,remove_group)
pheno <- exclude.samples(pheno,"X",samples.to.exclude)

# Get sample names
pheno$FullSampleName <- samples.with.gt(pheno,sm,"X","sample_names_oh_corrected",return.variable="FullSampleName")

# Check if sample was genotyped
pheno$isGenotyped <- !is.na(match.acc.names(pheno$FullSampleName,get.sample.names(gt)))

# Keep only lines that was genotyped
pheno.with.gt <- pheno[pheno$isGenotyped,]
nrow(pheno.with.gt) # number of lines genotyped

# Get genotypes of lines
gt.pheno <- subset_genotypes(gt,pheno.with.gt$FullSampleName)

# Traits to predict
traits <- names(pheno.with.gt)[2:19]

## Prepare phenotype and genotype of breeding line phenotypes from each state
# Add prefix to each state's phenotype data
colnames(oh.blues) <- sapply(colnames(oh.blues),add_prefix,"OH")
colnames(in.blues) <- sapply(colnames(in.blues),add_prefix,"IN")
colnames(ky.blues) <- sapply(colnames(ky.blues),add_prefix,"KY")
colnames(il.blues) <- sapply(colnames(il.blues),add_prefix,"IL")

# make column to preserve name of all locations
oh.blues[,"NAME"] <- oh.blues[,"OH.X"]
in.blues[,"NAME"] <- in.blues[,"IN.Lines"]
ky.blues[,"NAME"] <- ky.blues[,"KY.name"]
il.blues[,"NAME"] <- il.blues[,"IL.sample_name"]

# Merge all data
library(plyr)
merged <- rbind.fill(rbind.fill(rbind.fill(oh.blues,in.blues),ky.blues),il.blues)
cat(dim(merged)[1],"entries loaded!\n")

# OH yield
names(oh.blues)
oh.trt <- "OH.YLD"
hist(oh.blues[,oh.trt])
non.mis <- !is.na(oh.blues[,oh.trt])
y.cv <- oh.blues[non.mis,oh.trt]
oh.blues$FullSampleName <- samples.with.gt(oh.blues,sm,"OH.X","sample_names_oh_corrected",return.variable="FullSampleName")
oh.blues$isGenotyped <- !is.na(match.acc.names(oh.blues$FullSampleName,sample_names))
oh.bwgt <- oh.blues[oh.blues$isGenotyped,]
dim(oh.bwgt)
# gt.cv <- subset_genotypes(gt,oh.bwgt[,"FullSampleName"])

# CV traits
names(merged)
cv.trt <- c("OH.YLD","IN.YLD","KY.yld","IL.Grain.yield...bu.ac","OH.TW","IN.TW","KY.twt","IL.Grain.test.weight...lbs.bu")
cv.with.trt <- merged[rowSums(!is.na(merged[cv.trt])) > 0,]
dim(cv.with.trt)
colSums(!is.na(cv.with.trt[cv.trt]))
sample_names <- get.sample.names(gt)
cv.with.trt$FullSampleName <- samples.with.gt(cv.with.trt,sm,"NAME","sample_names_oh_corrected",return.variable="FullSampleName")
gtd.idx <- !is.na(match.acc.names(cv.with.trt$FullSampleName,sample_names))
cv.with.gt <- cv.with.trt[gtd.idx,]
gt.cv <- subset_genotypes(gt,cv.with.gt[,"FullSampleName"])
dim(gt.cv)
dim(cv.with.gt)

colSums(!is.na(cv.with.trt[,cv.trt]))
colSums(!is.na(cv.with.trt[gtd.idx,cv.trt]))

tmp.cv.df <- as.data.frame(matrix(nrow=nrow(cv.with.gt),ncol=length(pheno.with.gt)))
colnames(tmp.cv.df) <- colnames(pheno.with.gt)
tmp.cv.df[,"X"] <- cv.with.gt[,"NAME"]
pheno.bind.na <- rbind(pheno.with.gt,tmp.cv.df)
tmp.na.df <- as.data.frame(matrix(nrow=nrow(pheno.with.gt),ncol=length(cv.trt)))
colnames(tmp.na.df) <- cv.trt
pheno.bind <- rbind(tmp.na.df,cv.with.gt[cv.trt])
gt.bind <- rbind(gt.pheno,gt.cv)

## Analysis ##

# Run model for all traits
library(BGLR)
library(parallel)
yhats <- mclapply(pheno.bind.na[,traits],run_bglr,gt.bind,mc.cores=18)


# Aggregate results
pred <- do.call(cbind,yhats)
colnames(pred) <- paste("pred.",colnames(pred), sep="") # add prefix to results

# Get correlations
tw.cor.bl <- cor(pheno.bind[,5:8],pred[,1:9],use="pairwise.complete.obs")
yl.cor.bl <- cor(pheno.bind[,1:4],pred[,10:18],use="pairwise.complete.obs")
tw.cor <- cor(pheno.bind.na[,traits[1:9]],pred[,1:9],use="pairwise.complete.obs")
yl.cor <- cor(pheno.bind.na[,traits[10:18]],pred[,10:18],use="pairwise.complete.obs")

#pheno.cor <- cor(pheno.bind[,traits],pheno.bind.na[,traits],use="pairwise.complete.obs")

# Write outputs
write.csv(cbind(pheno.with.gt,pred),"output/5-state-predictions.csv")
write.csv(tw.cor,"output/5-state-twt-CV.csv")
write.csv(yl.cor,"output/5-state-yld-CV.csv")

# Run CV
nfold <- 10

run_mult_cv(1,2,c("yl.ALL","yl.OH"))
mult_nfold_CV <- lapply(c(1,2,5,10),run_cv_mult_nfold,c("yl.ALL","yl.OH"))
mult_nfold_cor <- do.call(rbind,mult_nfold_CV)
mult_nfold_cor

replicated_CV <- lapply(1:5,run_mult_cv,10,traits)
rCV_df <- do.call(rbind,replicated_CV)

#devtools::install_github("kassambara/ggpubr")
library(ggpubr)
library(reshape2)

corCV_mean <- apply(rCV_df,2,mean)
cv_out <- rbind(rCV_df,corCV_mean)
row.names(cv_out)[6] <- "mean"
write.csv(cv_out,"data/5-state-cv-cor.csv")

m <- melt(rCV_df)
names(m) <- c("rep","trait","accuracy")
p <- ggboxplot(m, x="trait", y="correlation",)
p + theme(axis.text.x = element_text(angle=-90, hjust=0))

# OH
OH.yhatCV <- run_cv(pheno.with.gt[,"yl.OH"],gt.pheno,10)
cor(predCV[,18],OH.yhatCV)
cor(pheno.with.gt[,"yl.OH"],OH.yhatCV,use="pairwise.complete.obs")
plot(pheno.with.gt[,"yl.OH"],OH.yhatCV)
