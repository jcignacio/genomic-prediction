#!/usr/bin/env Rscript
#install.packages("BGLR")
#install.packages("plyr")
library(BGLR)
library(tidyverse)
library(plyr)
library(readxl)
library(parallel)
library(rrBLUP)
library(gaston)

wd <- "/home/carli/workspace/genomic-prediction/"
setwd(wd)

source("src/kin.blup4.4.R")
source("src/gtutils.R")

load("~/workspace/phenotype-data-analysis/NorGrains-2021/output/melt_2021_plot_data.Rdata")
vcffile <- "data/all_regions_samp_filt_dedup_shortnames_imp.vcf.gz"
vcf <- read.vcf(vcffile, convert.chr = F)

# Get sample names
sample_names <- vcf@ped$id
matching_sample_names <- match.acc.names(as.character(md$germplasmName),sample_names)
with_geno <- !is.na(matching_sample_names)
matching_sample_names_with_geno <- matching_sample_names[with_geno]
md$germplasmName <- as.character(md$germplasmName)
md$germplasmName[with_geno] <- sample_names[matching_sample_names_with_geno]
md$germplasmName <- as.factor(md$germplasmName)


## Get marker data
X <- as.matrix(vcf)
X[1:10,1:10]

# Filter data based on LD
ldfile <- "all_regions_samp_filt_dedup_shortnames_imp.20mb.r30"
system(paste("plink","--vcf",vcffile,"--allow-extra-chr","--indep-pairwise 50 10 0.3","--out",ldfile))

## Filter marker data
ldfilt <- read.table(paste0(ldfile,".prune.in"))
X2 <- X[,ldfilt$V1]
dim(X2)

## Spatial Adjust
# library(SpATS)

# sites <- unique(md$studyName)
# site <- sites[5]
# dat <- md %>% filter(variable == trait)
# #dat <- dat[sample(1:nrow(dat),size = 100,replace=F),]
# dat$row <- as.numeric(dat$rowNumber)
# dat$col <- as.numeric(dat$colNumber)
# dat <- na.omit(dat)
# a1 <- SpATS(response = trait, spatial = ~ SAP(row, col, nseg = c(10,10), degree = 3, pord = 2), 
#             #genotype = "NAME", fixed = ~ 1, random = ~ R + C, data = sitedata, 
#             genotype = "germplasmName", fixed = ~ 1, random = ~ rowNumber + colNumber, data = dat %>% filter(studyName == site), 
#             control =  list(tolerance = 1e-03))


## Get BLUEs
library(lme4)
library(reshape2)

traits <- unique(md$variable)
trait <- traits[2]
locs <- c("IL","IN","KY","OH")
loc <- locs[4]

cv.list.out <- list()

run.cv <- function(trait, loc) {
  dat <- md %>% filter(variable == trait) %>% filter(grepl(paste0(loc,"$"),locationName))
  #dat <- dat[sample(1:nrow(dat),size = 100,replace=F),]
  m1 <- lmer(value ~ 0 + germplasmName + (1|studyName) + (1|replicate:studyName), data=dat)
  blu <- summary(m1)$coef
  rownames(blu) <- gsub("^germplasmName","",rownames(blu))
  blu <- as.data.frame(blu)
  blu[,"germplasmName"] <- rownames(blu)
  
  ## rrBLUP CVs
  
  matching_sample_names <- match.acc.names(rownames(blu),sample_names)
  blu2 <- blu[!is.na(matching_sample_names),]
  X3 <- X2[matching_sample_names[!is.na(matching_sample_names)],]
  blu2$germplasmName <- rownames(X3)
  sd <- blu2
  sd$Estimate <- sd$`Std. Error`
  
  cvout <- replicate(10,cv_rrblup_with_miss_geno(trait="Estimate",pheno = blu2,X.cv = X3,R = sd,geno = "germplasmName",nfold = 5))
  
  df.out <- data.frame(location=loc, trait=trait, cv.acc = cvout)
  return(df.out)
}

for(trait in traits){
  for(loc in locs){
    cv.list.out[[paste(loc,trait,sep = "_")]] <- tryCatch(
      {
        suppressWarnings(run.cv(trait,loc))
      },
      error=function(cond) {
        message("Here's the original error message:")
        message(cond)
        return(NA)
      },
      warning=function(cond){
        message("Here's the original warning message:")
        message(cond)
        return(NULL)
      },
      finally={
        message(paste("Done CV for",loc,trait))
      }
    )
  }
}

df.cv.out <- do.call(rbind,cv.list.out)
write.csv(df.cv.out,"output/2021_within_state_CV.csv")

### plot

library(ggpubr)

pdat <- na.omit(df.cv.out)

library(reshape2)

cv.df.summ <- dcast(pdat, location ~ trait, mean)
ggplot(data=pdat, aes(x=location,y=cv.acc)) + geom_boxplot() + facet_wrap(~trait)

cvs <- list()
cvs$twt <- read.csv("output/2021_rrblup_accross_state_CV_testweight.csv")[,2]
cvs$yld <- read.csv("output/2021_rrblup_accross_state_CV_yield.csv")[,2]
cvs$hd <- read.csv("output/2021_rrblup_accross_state_CV_headingdate.csv")[,2]
cvs$ht <- read.csv("output/2021_rrblup_accross_state_CV_plantheight.csv")[,2]
cvs
cvs.df <- do.call(cbind,cvs)

mcvs <- melt(cvs.df)
colnames(mcvs) <- colnames(pdat)
mcvs$trait <- traits[rep(1:4,10)[order(rep(1:4,10))]]
mcvs$location <- "ALL"
pdat2 <- as.data.frame(rbind(pdat,mcvs))
png("output/2021_rrblup_NIFA_states_CV.png")
ggplot(data=pdat2, aes(x=factor(location, level=c(locs,"ALL")),y=cv.acc)) + geom_boxplot() + facet_wrap(~trait) +
  ggtitle("5-fold, 10-rep CV accuracies of test weight, yield, heading date,\nand plant height within and across NIFA states") +
  xlab("state") + ylab("CV accuracy")
dev.off()


all.states.cv <- dcast(mcvs, "ALL" ~ trait, mean)
colnames(all.states.cv) <- colnames(cv.df.summ)
cvs.df2 <- rbind(cv.df.summ,all.states.cv)
write.csv(cvs.df2,"output/2021_rrblup_across_and_within_state_mean_CVs.csv")
write.csv(pdat2,"output/2021_rrblup_across_and_within_state_10_rep_CVs.csv",row.names = F)
