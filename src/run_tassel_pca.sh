#!/bin/bash

#  Download TASSEL 5 standalone from bitbucket and set run_pipeline.pl path
tassel=~/softwares/tassel-5-standalone/run_pipeline.pl 

cd /users/PAS1286/jignacio/projects/nifa-genomic-prediction/data

ln -s /users/PAS1286/jignacio/shared/shared/gbs_data/nifa_2020/NIFA_North_FHB_postimp_filt_no_chr2B.vcf.gz

# Run in OSC with this command
srun -c24 $tassel -Xmx24g -vcf data/NIFA_North_FHB_postimp_filt_no_chr2B.vcf.gz -PrincipalComponentsPlugin -covariance true -endPlugin -export output/pca_output &
