#!/usr/bin/env Rscript
#install.packages("BGLR")
#install.packages("dplyr")
library(BGLR)
library(plyr)
#library(breedbase)
library(dplyr)
library(parallel)

locs <- c("IL","OH","KY","IN")

#oh_phenotype_file <- "oh_blups.txt"
oh_phenotype_file <- "data/oh_blups.csv"
in_phenotype_file <- "data/in_blups.csv"
ky_phenotype_file <- "data/ky_blues_2020.csv"
il_phenotype_file <- "data/il_blups.csv"
key_file <- "data/sample_keys.csv"

args = commandArgs(trailingOnly=TRUE)
if(length(args) == 0){
  wd <- "~/workspace/genomic-prediction"
  setwd(wd)
  nfold <- 10
  prefix <- "genfreq"
}else{
  prefix <- args[1]
  nfold <- as.numeric(args[2])
}
filter_lines <- "data/line_filter-NIFA_PCA_2020-10-29.csv"
encoded_gt_zip <- paste0("tmp/",prefix,".gt.enc.corrected.txt.zip")
encoded_gt <- paste0("tmp/",prefix,".gt.enc.corrected.txt")

# list.files()
cat("Loading phenotypes... ")
oh_pheno <- read.csv(oh_phenotype_file,sep=",",stringsAsFactors = F, header = T)
in_pheno <- read.csv(in_phenotype_file,sep="\t",stringsAsFactors = F, header = T)
ky_pheno <- read.csv(ky_phenotype_file,sep=",",stringsAsFactors = F, header = T)
il_pheno <- read.csv(il_phenotype_file,sep=",",stringsAsFactors = F,header=T)
line_filt <- read.csv(filter_lines,stringsAsFactors = F,header=T, row.names = 1)
row.names(line_filt) <- NULL

# head(oh_pheno)
# head(in_pheno)
# head(ky_pheno)
# head(il_pheno)

## Resolve headers
# add prefix in header names
#name <- "carlos"; prefix <- "dr"; delimiter="."
add_prefix <- function(name, prefix, delimiter="."){
  paste0(c(prefix,name),collapse = ".")
}

colnames(oh_pheno) <- sapply(colnames(oh_pheno),add_prefix,"OH")
colnames(in_pheno) <- sapply(colnames(in_pheno),add_prefix,"IN")
colnames(ky_pheno) <- sapply(colnames(ky_pheno),add_prefix,"KY")
colnames(il_pheno) <- sapply(colnames(il_pheno),add_prefix,"IL")

# add prefix to accession names
oh_pheno[,"UNIQUE.NAME"] <- sapply(oh_pheno[,"OH.X"],add_prefix,"OH")
in_pheno[,"UNIQUE.NAME"] <- sapply(in_pheno[,"IN.Lines"],add_prefix,"IN")
ky_pheno[,"UNIQUE.NAME"] <- sapply(ky_pheno[,"KY.name"],add_prefix,"KY")
il_pheno[,"UNIQUE.NAME"] <- sapply(il_pheno[,"IL.sample_name"],add_prefix,"IL")

# make column to preserve name of all locations
oh_pheno[,"NAME"] <- oh_pheno[,"OH.X"]
in_pheno[,"NAME"] <- in_pheno[,"IN.Lines"]
ky_pheno[,"NAME"] <- ky_pheno[,"KY.name"]
il_pheno[,"NAME"] <- il_pheno[,"IL.sample_name"]

# Merge all data
merged <- rbind.fill(rbind.fill(rbind.fill(oh_pheno,in_pheno),ky_pheno),il_pheno)
cat(dim(merged)[1],"entries loaded!\n")
# colnames(merged)

# Make output names from key file
kf <- read.csv(key_file,sep=",",stringsAsFactors = F,header=T)
kf$names_out <- kf$FullSampleName
oh.samples.idx <- kf$pi_or_coordinator == "Clay Sneller"
kf$names_out[oh.samples.idx] <- kf$sample_names_oh_corrected[oh.samples.idx]

# Read and check encoded genotype file
source("src/gtutils.R")

if(!file.exists(encoded_gt)){
  unzip(zipfile=encoded_gt_zip, exdir="./data/")
}
cat("Loading genotype file... ")
enc <- read.table(encoded_gt,header=F,sep="\t",comment.char = "%", stringsAsFactors = F)
cat(dim(enc)[2]-1, "samples and", dim(enc)[1]-12, "markers loaded!\n")

### Prepare GS dataset
cat("Preparing GS dataset...\n")

# Get sample names
sample_names <- get.sample.names(enc)

# Match phenotypes with genotypes
matching_sample_names <- match.acc.names(merged$"NAME",sample_names)

## Trim dataset for training
# Remove phenotyped lines with no genotypes
pheno_with_gt <- merged[!is.na(matching_sample_names),]

in.pheno.idx <- grep("^IN",merged[,"UNIQUE.NAME"])
in.fhb.idx <- in.pheno.idx[!is.na(merged[in.pheno.idx,"IN.INC"])]
match(sample_names, merged[,"NAME"])

#Filter genotype based on line_filt
# pheno_with_gt <- pheno_with_gt[match(line_filt$name, pheno_with_gt$NAME),]
# pheno_with_gt <- pheno_with_gt[!is.na(pheno_with_gt$UNIQUE.NAME),]
# row.names(pheno_with_gt) <- NULL

# Get marker data for phenotypes
t.enc <- prepare_input_gt(enc)
gt.idx <- matching_sample_names[!is.na(matching_sample_names)]
X <- t.enc[gt.idx,]

# Remove outliers
rm_outliers <- function(dat){
  dat[dat %in% boxplot.stats(dat)$out] <- NA
  return(dat)
}
pheno_with_gt[,"IL.tst.wgt"] <- rm_outliers(pheno_with_gt$IL.Grain.test.weight...lbs.bu)
pheno_with_gt[,"IN.tst.wgt"] <- rm_outliers(pheno_with_gt$IN.TW * 0.0597)
pheno_with_gt[,"KY.tst.wgt"] <- rm_outliers(pheno_with_gt$KY.twt)
pheno_with_gt[,"OH.tst.wgt"] <- rm_outliers(pheno_with_gt$OH.TW)

# Merge phenotypes from diffrent states into 1 column
#headers <- c('OH.YLD","IN.YLD.tonsHA","KY.yld","IL.Grain.yield...bu.ac')
trait_name <- 'tst.wgt'
id.col <-  'UNIQUE.NAME'
y1 <- pheno_with_gt %>% mutate( mycol = coalesce(IL.tst.wgt
                                                 #,IL.FHB.severity.....
                                                 #,IN.INC
                                                 ,IN.tst.wgt
                                                 #,IL.FHB.incidence.....
                                                 ,KY.tst.wgt
                                                 ,OH.tst.wgt
                                                 )) %>%
  select(!!enquo(id.col), mycol)
colnames(y1) <- c(id.col,trait_name)
pheno_with_gt[,trait_name] <- y1[match(pheno_with_gt$UNIQUE.NAME,y1$UNIQUE.NAME),trait_name]
#y1 <- y1[!is.na(y1[,trait_name]),]

# Set groups
mtc.idx <- match(y1[,id.col],merged[,id.col])
y1[,"state"] <- as.factor(substr(merged[mtc.idx,id.col],1,2))
y1[,"group"] <- as.numeric(y1[,"state"])
pheno_with_gt[,trait_name] <- y1[,trait_name]
pheno_with_gt[,"group"] <- y1[,"group"]

# Visualize merged phenotypes
png(paste0("output/CV_across_groups_filteredPC_observed_",trait_name,".png"))
plot(y1[,trait_name],col=y1$group,main=paste0("Observed ",trait_name," in 4 states"),ylab=trait_name,xlab="lines")
legend("topright",legend=levels(y1$state)[unique(y1$group)],pch=1,col=unique(y1$group))
dev.off()

# Set list of traits to analyze
names(pheno_with_gt)
traits <- c("IL.tst.wgt","IN.tst.wgt","KY.tst.wgt","OH.tst.wgt",trait_name)

# Make output data.frame
out_df <- data.frame(UNIQUE.NAME = pheno_with_gt$UNIQUE.NAME)

# Run BGLR
for(trt in traits){
  out_df[trt] <- pheno_with_gt[trt]
  out_df[,paste("pred",trt,sep = ".")] <- run_bglr(pheno_with_gt[,trt],X,T)
}

cor_out <- cor(out_df[c(traits,paste("pred",traits,sep = "."))],use = "pairwise.complete.obs")
View(cor_out)

# Specify trait
cv.y <- pheno_with_gt[!is.na(pheno_with_gt$IL.tst.wgt),"IL.tst.wgt"]
cv.x <- X[!is.na(pheno_with_gt$IL.tst.wgt),]

# Run CV
yhat <- run_cv(cv.y,cv.x,10)

# Get accuracy
cor(cv.y,yhat)

runBGLR <- function(trait, pheno_with_gt, out_df, fixed.effect=NULL, random.effect=NULL){
  # df.y <- data.frame(unique.name = out_df$unique.name
  #                    ,sample.name.old = out_df$sample.name.old)
  # df.y[,trait] <- pheno_with_gt[match(out_df$unique.name,pheno_with_gt$UNIQUE.NAME),trait]
  # tmp <- df.y[!is.na(df.y[,trait]),]
  # y <- as.numeric(tmp[match(sample_names,tmp$sample.name.old),trait])
  # idx.non.na.y <- which(!is.na(y))
  # W <- X[idx.non.na.y,]
  # y <- y[idx.non.na.y]
  # folds <- tmp[idx.non.na.y,"groups"]
  # #df.y <- trimDuplicates(pheno_with_gt,trait,sample_names)
  # n <- length(y)
  # #y <- as.numeric(df.y[,trait])
  
  pheno_with_gt<- pheno_with_gt[!is.na(pheno_with_gt[,trait]),]
  
  y <- pheno_with_gt[,trait]
  n <- length(y)
  folds <- sample(nfold,length(pheno_with_gt$group),replace=TRUE)
  idx.non.na.y <- match(pheno_with_gt$NAME,sample_names)
  W <- X[match(pheno_with_gt$NAME,sample_names),]
  
  # Run model
  #folds=sample(1:nfold,size=n,replace=T)
  cat("Running BGLR CV for",trait,"... ")
  time <- system.time( tmp<-mclapply(FUN=fitFold,y=y,W=W,folds=folds,X=1:max(folds),mc.cores=nfold))[3]
  yHatCV2=unlist(tmp)
  yHatCV2=yHatCV2[order(as.integer(names(yHatCV2)))] # need to order the vector
  cat("Completed in",round(as.numeric(time)/60,digits = 2),"minutes.\n")
  
  out_df2 <- data.frame(pheno_with_gt$UNIQUE.NAME,sample_names[idx.non.na.y],y,yHatCV2)
  colnames(out_df2) <- c("unique.names","sample.names",paste0(trait,".obs"),paste0(trait,".prd"))
  
  R <- cor(y,yHatCV2,use="complete.obs")
  
  png(paste0("output/CV_across_groups_filteredPC_",trait,".png"))
  plot(y,yHatCV2, xlab="observed", ylab="predicted", main=paste0("Between-states validation of ",trait,"\nn=",n, ", R^2=",round(R^2,2)), col=folds)
  legend("topright",legend=1:nfold,pch=1,col=1:nfold)
  dev.off()
  
  out_list <- list(df=out_df2,R=R)
  return(out_list)
}

# First create a function that fits one fold
fitFold=function(y,W,folds,fold){
  tst=which(folds==fold)
  yNA=y
  yNA[tst]=NA
  fm=BGLR(y=yNA,ETA=list(list(X=W,model='BRR')),nIter=1500,burnIn=500,verbose=F)
  tmp=fm$yHat[tst]
  names(tmp)=tst
  return(tmp)
}

out_folder <- paste0("output")
if(!dir.exists(out_folder)){
  dir.create(out_folder)
}

# Run BGLR for all traits and write predictions to csv file

corlist <- list()

for(trait in trait){
  out_list <- tryCatch(runBGLR(trait, pheno_with_gt, out_df),error=function(e){cat("Error running lmer for",trait,":",conditionMessage(e), "\n"); return(out_list)})
  out_df3 <- out_list$df
  corlist[[trait]] <- out_list$R
  write.csv(out_df3,paste0("output/CV_across_groups_filteredPC_",trait,"_predictions.csv"),row.names = F)
}


# Write correlations
out_corlist <- do.call(rbind,corlist)
colnames(out_corlist) <- c("correlation_coefficient")
write.csv(out_corlist,paste0("output/CV_across_groups_filteredPC_",trait,"_correlations.csv"))

# # check corellation between traits
# out_df <- read.csv("output_predictions.csv")
# traits
# for(i in 4:8){
#   for(j in 4:8){
#     print(paste(colnames(out_df)[i],"x",colnames(out_df)[j],":",cor(out_df[,i],out_df[,j])))
#   }
# }
# 
# gs_out <- data.frame(sample_name = sample_names, gs_prediction = fm$yHat)
# gs_out[grep("OH15-191-52",gs_out[,1],ignore.case = T),]
# asc <- gs_out[order(gs_out$gs_prediction),"sample_name"]
# out_df[match(asc,out_df$sample.name.old),4]
# oh_pheno[match(asc,oh_pheno$OH.NAME),trait]
# pred <- gs_out[match(oh_pheno$OH.NAME,gs_out$sample_name),2]
# na.pred.idx <- is.na(pred)
# na.trait.idx <- is.na(oh_pheno[,trait])
# na.both.idx <- as.logical((!na.pred.idx) * (!na.trait.idx))
# na.both.idx
# cor(pred[na.both.idx],oh_pheno[na.both.idx,trait])
# head(oh_pheno)
