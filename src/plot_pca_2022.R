library(ggfortify)

pcx <- prcomp(X, rank.=5)

pcx2 <- prcomp(X2,rank.=5)

pcx3 <- prcomp(X3,rank.=5)

X4 <- X[,-grep("S2B",colnames(X))]
dim(X4)

pcx4 <- prcomp(X4)

X5 <- X4[,-grep("S7D",colnames(X4))]
dim(X5)
pcx5 <- prcomp(X5) 

# biplot(pcx2,arrow.len=0)
# plot(pcx2$x[,1],pcx2$x[,2])
# plot(pcx2$x[,2],pcx2$x[,3])

# sn <- rownames(X2)
# pre <- substr(sn,1,2)
# tb.sn <- table(pre)
# keep.names <- names(tb.sn)[tb.sn > 10]
# pre[!pre %in% keep.names] <- "others"
# pre <- as.factor(pre)

autoplot(pcx, data = dfs, colour = 'State', label=T, shape=F, main="Unfiltered PC1 vs PC2")
autoplot(pcx, data = dfs, colour = 'State', label=T, shape=F, main="Unfiltered PC2 vs PC3", x=2, y=3)

autoplot(pcx3, data = dfs, colour = 'State', label=T, shape=F, main="HWE filtered PC1 vs PC2")
autoplot(pcx3, data = dfs, colour = 'State', label=T, shape=F, main="HWE filtered PC2 vs PC3", x=2, y=3)

autoplot(pcx2, data = dfs, colour = 'State', label=T, shape=F, main="LD filtered PC1 vs PC2")
autoplot(pcx2, data = dfs, colour = 'State', label=T, shape=F, main="LD filtered PC2 vs PC3", x=2, y=3)

autoplot(pcx4, data = dfs, colour = 'State', label=T, shape=F, main="No Chr 2B PC1 vs PC2")
autoplot(pcx4, data = dfs, colour = 'State', label=T, shape=F, main="No Chr 2B PC2 vs PC3", x=2, y=3)
autoplot(pcx5, data = dfs, colour = 'State', label=T, shape=F, main="No Chr 2B & 7D PC1 vs PC2")
autoplot(pcx5, data = dfs, colour = 'State', label=T, shape=F, main="No Chr 2B & 7D PC2 vs PC3", x=2, y=3)

library(reshape2)
mpc <- cbind(data.frame(pcx$x),Dataset="Unfiltered",State=dfs$State)
mpc <- rbind(mpc,cbind(data.frame(pcx3$x),Dataset="HWE filtered",State=dfs$State))
mpc <- rbind(mpc,cbind(data.frame(pcx2$x),Dataset="LD filtered",State=dfs$State))

ggplot(mpc, aes(x=PC1,y=PC2, group=State, color=State)) + geom_point() + facet_wrap(~Dataset, scales="free")
ggplot(mpc, aes(x=PC2,y=PC3, group=State, color=State)) + geom_point() + facet_wrap(~Dataset, scales="free")

sum(pcx3$x[,"PC1"] > 0)
sum(pcx2$x[,"PC1"] > 0)

plot(pcx2$x[,1],pcx2$x[,2],col=pre)
legend("topleft",levels(pre),col=1:length(levels(pre)),pch=1)
plot(pcx2$x[,2],pcx2$x[,3],col=pre)
legend("topleft",levels(pre),col=1:length(levels(pre)),pch=1)
plot(pcx2$x[,1],pcx2$x[,3],col=pre)
legend("topleft",levels(pre),col=1:length(levels(pre)),pch=1)

pcx2$rotation[order(pcx2$rotation[,1],decreasing=F),]
pcx2$rotation[order(pcx2$rotation[,1],decreasing=T),]
pcx2$rotation[order(pcx2$rotation[,3],decreasing=T),]

p2 <- as.data.frame(pcx$rotation)

p3 <- cbind(p2,do.call(rbind,str_split(rownames(p2),"_")))
p3 <- as.data.frame(p3)
names(p3)[6:7] <- c("chr","pos")
p3$chrnum <- substr(p3$chr,1,2)
p3$genome <- substr(p3$chr,3,3)

p4 <- melt(p3)
p3$pos <- as.numeric(p3$pos)
p3$mb <- p3$pos/1000000
p4$pos <- as.numeric(p4$pos)
p4$mb <- p4$pos/1000000

summary(p3)
summary(p4)

ggplot(p3, aes(x=mb,y=PC1)) + geom_line() + facet_wrap(~chr,ncol=3)
ggplot(p3, aes(x=mb,y=PC1)) + geom_line() + geom_point() + facet_wrap(~chr,ncol=3)
ggplot(p3, aes(x=mb,y=PC2)) + geom_line() + facet_wrap(~chr,ncol=3)
ggplot(p3, aes(x=mb,y=PC3)) + geom_line() + facet_wrap(~chr,ncol=3)

ggplot(p4, aes(x=mb,y=value,group=variable, color=variable)) + geom_line() + facet_wrap(~chr,ncol=3)
